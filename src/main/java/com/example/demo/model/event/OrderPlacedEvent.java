package com.example.demo.model.event;

public class OrderPlacedEvent {

    private final String orderId;

    public OrderPlacedEvent(final String orderId) {
        this.orderId = orderId;
    }

    //FIXME: should fail here
//    public String getOrderId() {
//        return orderId;
//    }


}
