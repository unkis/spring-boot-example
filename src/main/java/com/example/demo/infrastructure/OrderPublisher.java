package com.example.demo.infrastructure;

import com.example.demo.model.event.OrderPlacedEvent;

public interface OrderPublisher {
    void publish(OrderPlacedEvent orderPlacedEvent);
}
