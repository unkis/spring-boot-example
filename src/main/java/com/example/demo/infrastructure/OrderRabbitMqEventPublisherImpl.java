package com.example.demo.infrastructure;

import com.example.demo.model.event.OrderPlacedEvent;

import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

// FIXME: should fail here
//@Component
public class OrderRabbitMqEventPublisherImpl implements OrderPublisher {

    private final Source source;

    public OrderRabbitMqEventPublisherImpl(final Source source) {
        this.source = source;
    }

    @Override
    public void publish(final OrderPlacedEvent orderPlacedEvent) {
        Map<String, Object> headers = new HashMap<>();
        headers.put("type", orderPlacedEvent.getClass().getName());
        // FIXME: should fail here
//        this.source.output().send(new GenericMessage<>(orderPlacedEvent,headers));
    }
}
