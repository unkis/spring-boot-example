package contracts.tse

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	label("order_placed")
	input {
		triggeredBy("sendAccepted()")
	}
	outputMessage {
		sentTo("channel")
		body(
			orderId: "ddd",
		)
		headers {
			header("type", "order-placed")
			messagingContentType(applicationJson())
		}
	}
}