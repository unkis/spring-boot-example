package de.lieferplatz.pizzaprocloud.contract;

import com.example.demo.infrastructure.OrderRabbitMqEventPublisherImpl;
import com.example.demo.model.event.OrderPlacedEvent;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.test.binder.MessageCollectorAutoConfiguration;
import org.springframework.cloud.stream.test.binder.TestSupportBinderAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = TseBase.Config.class)
@AutoConfigureMessageVerifier
public class TseBase {
	private static final String VALID_PERSONAL_ID = "86010197600";
	private static final String INVALID_PERSONAL_ID = "86010156812";

	@Autowired
	private OrderRabbitMqEventPublisherImpl publisher;

	public void sendAccepted() {
		//FIXME: should fail, wrong orderId
		publisher.publish(new OrderPlacedEvent("test_order_id"));
	}

	@Configuration
	@ImportAutoConfiguration({ TestSupportBinderAutoConfiguration.class,
			MessageCollectorAutoConfiguration.class, JacksonAutoConfiguration.class })
	@EnableBinding(Source.class)
	static class Config {
		@Bean
		OrderRabbitMqEventPublisherImpl publisher(Source source) {
			return new OrderRabbitMqEventPublisherImpl(source);
		}
	}
}